import { useEffect, useState } from "react";
import { Amplify } from "aws-amplify";
import { FaceLivenessDetector } from "@aws-amplify/ui-react-liveness";
import { Loader, ThemeProvider } from "@aws-amplify/ui-react";
import awsexports from "./aws-exports";

import "@aws-amplify/ui-react/styles.css";

const CONFIDENCE_THRESHOLD = 0.8;

export function LivenessQuickStartReact() {
  const [loading, setLoading] = useState(false);
  const [sessionId, setSessionId] = useState<string | null>(null);

  useEffect(() => {
    const fetchCreateLiveness = async () => {
      try {
        const request = await fetch(
          "https://preprod.api.sniffies.com/api/liveness",
        );
        const data = (await request.json()) as { SessionId: string };
        console.log({ data });

        setSessionId(data.SessionId);
        setLoading(false);
      } catch (error) {
        console.error(error);
      }
    };

    fetchCreateLiveness();
  }, []);

  const handleAnalysisComplete = async () => {
    const response = await fetch(
      `https://preprod.api.sniffies.com/api/liveness`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ sessionId }),
      },
    );
    const data = await response.json();

    console.log(data);
    if (data.Confidence > CONFIDENCE_THRESHOLD) {
      console.log("User is live");
    } else {
      console.log("User is not live");
    }
  };

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
        width: "100vw",
      }}
    >
      <ThemeProvider>
        {loading && <Loader />}

        {!loading && sessionId && (
          <FaceLivenessDetector
            sessionId={sessionId}
            region="us-east-1"
            onAnalysisComplete={handleAnalysisComplete}
            onError={(error) => {
              console.error(error);
            }}
            disableStartScreen={true}
          />
        )}
      </ThemeProvider>
    </div>
  );
}

Amplify.configure(awsexports);

export default function App() {
  return <LivenessQuickStartReact />;
}
