import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";

const port = parseInt(process?.env?.PORT ?? "4200", 10);

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    port,
  },
  plugins: [react({})],
});
